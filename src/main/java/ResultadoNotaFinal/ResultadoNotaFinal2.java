/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ResultadoNotaFinal;

import javax.swing.JOptionPane;


class ResultadoNotaFinal2 {    
    public static void Nota() {
        int nota = Integer.parseInt(JOptionPane.showInputDialog(null, 
         "Ingrese nota final: ","Nota Final",JOptionPane.QUESTION_MESSAGE));
        String resultado = "";
        if( nota >=0 && nota <=12){
            resultado = "Desaprobado";
        } else if (nota>=13 && nota<=15) {
            resultado = "Regular";
        } else if (nota>=16 && nota<=18) {
            resultado = "Bueno";
        } else if (nota>=19 && nota <=20) {
            resultado = "Escelente";
        } else {
            resultado = "Nota Invalida";
        }
    
        JOptionPane.showMessageDialog(null,"Su resultado es: " + resultado,
        "Estado en el curso",JOptionPane.INFORMATION_MESSAGE);
    }
    
}
