package app06;

import javax.swing.JOptionPane;

public class Funciones_void {
    public void Saludo(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje);
    }
    public void Sumar(double num1, double num2, String operador){
        double resultado = num1 + num2;
        JOptionPane.showMessageDialog(null,"La "+operador+" de "+num1+" y "+num2+" es: "+resultado);
    }
    public void Restar(double num1, double num2, String operador){
        double resultado = num1 - num2;
        JOptionPane.showMessageDialog(null,"La "+operador+" de "+num1+" y "+num2+" es: "+resultado);
    }
    public void Multiplicar(double num1, double num2, String operador){
        double resultado = num1 * num2;
        JOptionPane.showMessageDialog(null,"La "+operador+" de "+num1+" y "+num2+" es: "+resultado);
    }
    public void Dividir(double num1, double num2, String operador){
        double resultado = num1 / num2;
        JOptionPane.showMessageDialog(null,"La "+operador+" de "+num1+" y "+num2+" es: "+resultado);
    }
    public void Mayor(double num1, double num2){
        if (num1 > num2) {
            JOptionPane.showMessageDialog(null, "El número mayor es: "+num1);
        } else {
            JOptionPane.showMessageDialog(null, "El número mayor es: "+num2);
        }
    }
    public void Mayor(double num1, double num2, double num3){
        if (num1 > num2 && num1 > num3) {
            JOptionPane.showMessageDialog(null, "El número mayor es: "+num1);
        } else if (num2 > num1 && num2 > num3) {
            JOptionPane.showMessageDialog(null, "El número mayor es: "+num2);
        } else {
            JOptionPane.showMessageDialog(null, "El número mayor es: "+num3);
        }
    }
}
