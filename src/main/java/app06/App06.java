package app06;

import funciones_return.Funciones_return;
import javax.swing.JOptionPane;

public class App06 {
    public static void main(String[] args) {
        Funciones_return funciones_return = new Funciones_return();
        Funciones_void funciones_void = new Funciones_void();
        funciones_void.Saludo("Calculator perteneciente a:\n Mario Villanueva");
        int opcion = funciones_return.opcionesMenu();
        /*int opcion = Integer.parseInt(JOptionPane.showInputDialog("Elija una opcion: \n\n"
                    + " 1 - sumar \n"
                    + " 2 - restar \n"
                    + " 3 - multiplicar \n"
                    + " 4 - dividir \n"
                    + " 5 - mayor \n"
                    + " 6 - mayor de 3 numeros \n"));*/
        double num1 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese Numero 1"));
        double num2 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese Numero 2"));
        switch (opcion){
            case 1:
                funciones_void.Sumar(num1,num2,"sumar");break;
            case 2:
                funciones_void.Restar(num1,num2,"restar"); break;
            case 3:
                funciones_void.Multiplicar(num1,num2,"multiplicar"); break;
            case 4:
                funciones_void.Dividir(num1,num2,"dividir"); break;
            case 5:
                double num03 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese Numero 3"));
                JOptionPane.showMessageDialog(null, "El mayor es: " +
                        funciones_return.mayor(funciones_return.mayor(num1, num2),num03));
                //JOptionPane.showMessageDialog(null, "El mayor es: " + funciones_return.mayor(num1, num2));
                //funciones_void.Mayor(num1, num2);
                break;
            case 6:
                double num3 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese Numero 3"));
                funciones_void.Mayor(num1, num2, num3);
                break;
        }
        
        /*double resultado = 0;
        String operador = "";
        JOptionPane.showMessageDialog(null,"Bienvenido A Calculator \n Haga clic en Aceptar para continuar");
        int opcion = Integer.parseInt(JOptionPane.showInputDialog("Elija una opcion: \n\n"
                    + " 1 - sumar \n"
                    + " 2 - restar \n"
                    + " 3 - multiplicar \n"
                    + " 4 - dividir \n"));
        double num1 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese Numero 1"));
        double num2 = Double.parseDouble(JOptionPane.showInputDialog("Ingrese Numero 2"));
        switch (opcion){
            case 1:
                resultado = num1 + num2; operador = "Suma"; break;
            case 2:
                resultado = num1 - num2; operador = "Resta"; break;
            case 3:
                resultado = num1 * num2; operador = "Multiplicacion"; break;
            case 4:
                resultado = num1 / num2; operador = "Division"; break;
        }
        JOptionPane.showMessageDialog(null,"La "+operador+" de "+num1+" y "+num2+" es: "+resultado);*/
    }
}
