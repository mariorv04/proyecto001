package funciones_return;

import javax.swing.JOptionPane;

public class Funciones_return {
    public double  mayor (double num1, double num2){
        if (num1 > num2) {
            return num1;
        } else {
            return num2;
        }
    }
    public int opcionesMenu(){
        int opcion = Integer.parseInt(JOptionPane.showInputDialog("Elija una opción: "
                + " 1 - sumar \n"
                + " 2 - restar \n"
                + " 3 - multiplicar \n"
                + " 4 - dividir \n"
                + " 5 - mayor \n"));
        return opcion;
    }
    public double pedirDatos(){
        double num = Double.parseDouble(JOptionPane.showInputDialog("Ingrese Numero"));
        return num;
    }
}
