package app1;

import javax.swing.JOptionPane;

public class bucles02 {
    public static void main(String[] args) {
        boolean entra=true;
        JOptionPane.showMessageDialog(null,"Bienvenido a TableGenerator \n Haga clic en Aceptar para continuar");
        while(entra){
            int num = Integer.parseInt(JOptionPane.showInputDialog("Ingresa # de Tabla a Generar"));
            if (num == 0) {
                JOptionPane.showMessageDialog(null, "Gracias por utilizar TableGenerator","Acerca de ...",JOptionPane.QUESTION_MESSAGE);
                break;
            }
            int cont=1;
            String tabla = "";
            while(cont <= 12){
                tabla = tabla + cont + " X " + num + " = " + cont*num + "\n";
                cont++;
            }
            JOptionPane.showMessageDialog(null, "Tabla del "+ num +" es \n"+ tabla, "Resultados",JOptionPane.INFORMATION_MESSAGE);
        }

    }
}
