package app1;

import javax.swing.JOptionPane;

public class bucles01 {
    public static void main(String[] args) {
        int num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese Tabla a Generar"));
        int cont = 1;
        String tabla = "";
        while(cont <= 12){
            tabla = tabla + cont + " X " + num + " = " + cont*num + "\n";
            cont++;
        }
        JOptionPane.showMessageDialog(null, "Tabla del "+num+" es \n"+tabla);
    }
}
