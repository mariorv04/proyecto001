package app1;

import javax.swing.JOptionPane;

public class bucles07 {
    public static void main(String[] args) {
        boolean entra=true;
        JOptionPane.showMessageDialog(null,"Bienvenido a TableGenerator \n Haga clic en Aceptar para continuar");
        while(entra){
            int num = Integer.parseInt(JOptionPane.showInputDialog("Ingresa # de Tabla a Generar"));
            if (num < 0) {
                JOptionPane.showMessageDialog(null, "No puede ingresar números negativos","Acerca de ...",JOptionPane.ERROR_MESSAGE);
            } else if (num > 12 || num < 1){
                JOptionPane.showMessageDialog(null, "No puede ingresar números menores a 1 ni mayores a 12","Acerca de ...",JOptionPane.ERROR_MESSAGE);
            } else {  
                int cont;
                String tabla = "";
                for (cont = 1;  cont <= 12; cont++) {
                    tabla = tabla + cont + " X " + num + " = " + cont*num + "\n";
                }
                JOptionPane.showMessageDialog(null, "Tabla del "+ num +" es \n"+ tabla, "Resultados",JOptionPane.INFORMATION_MESSAGE);
            }
                
            int resp=JOptionPane.showConfirmDialog(null, "¿Desea generar otra tabla?","",JOptionPane.OK_CANCEL_OPTION);
            if (JOptionPane.OK_OPTION != resp) {
                JOptionPane.showMessageDialog(null, "Gracias por utilizar TebleGenerator Mario","Acerca de ...",JOptionPane.QUESTION_MESSAGE);
                break;
            }
        }
    }
}
