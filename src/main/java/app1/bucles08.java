package app1;

import javax.swing.JOptionPane;

public class bucles08 {
    public static void main(String[] args) {
        boolean entra = true;
        String[] tablas ={"1","2","3","4","5","6","7","8","9","10","11","12"};
         JOptionPane.showMessageDialog(null,"Bienvenidos a TableGenarator \n Haga clic en Aceptar para continuar");
         while(entra){
            String elegido = (String)JOptionPane.showInputDialog(null,"Seleccione # de Tabla a Generar Mario","COLORES",JOptionPane.QUESTION_MESSAGE, 
                    null,tablas,"Seleccione");
            int num = Integer.parseInt(elegido);
            int cont;
            String tabla = "";
             for (cont = 1; cont <= 12; cont++) {
                tabla = tabla + cont+" X "+num+" = "+ cont*num + "\n";
             }
             JOptionPane.showMessageDialog(null, "Tabla del "+ num +" es \n"+ tabla, "Resultados",JOptionPane.INFORMATION_MESSAGE);
             
            int resp=JOptionPane.showConfirmDialog(null, "¿Desea generar otra tabla?","",JOptionPane.OK_CANCEL_OPTION);
            if (JOptionPane.OK_OPTION != resp) {
                JOptionPane.showMessageDialog(null, "Gracias por utilizar TebleGenerator Mario","Acerca de ...",JOptionPane.QUESTION_MESSAGE);
                break;}
         }
    }
}
