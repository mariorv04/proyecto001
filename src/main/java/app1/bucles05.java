package app1;

import javax.swing.JOptionPane;

public class bucles05 {
    public static void main(String[] args) {
        int num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese # de la Tabla a Generar Mario"));
        int cont;
        String tabla = "";
        for ( cont = 1; cont <= 12; cont++) {
            tabla = tabla + cont + " X " + num + " = " + cont*num + "\n";
        }
        JOptionPane.showMessageDialog(null,"Tabla del "+num+" es \n"+tabla, "Resultado", JOptionPane.INFORMATION_MESSAGE);
    }
}
