package app1;

import javax.swing.JOptionPane;

public class bucles06 {
    public static void main(String[] args) {
        boolean entra = true;
        JOptionPane.showMessageDialog(null, "Bienvenido a TableGenerator \n Haga clic en Aceptar para cOntinuar");
        while(entra){
            int num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese # de la Tabla a Generar Mario"));
            int cont;
            String tabla = "";
            for ( cont = 1; cont <= 12; cont++) {
                tabla = tabla + cont + " X " + num + " = " + cont*num + "\n";
            }
            JOptionPane.showMessageDialog(null,"Tabla del "+num+" es \n"+tabla, "Resultado", JOptionPane.INFORMATION_MESSAGE);
            
            int resp = JOptionPane.showConfirmDialog(null,"¿Desea generar otra tabla?","",JOptionPane.OK_CANCEL_OPTION);
            if (JOptionPane.OK_OPTION != resp) {
                JOptionPane.showMessageDialog(null,"Gracias por utilizar TableGeneratos Mario","Acerca de...",JOptionPane.QUESTION_MESSAGE);
                break;
            }
        }
    }
}
