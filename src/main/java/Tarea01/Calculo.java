package Tarea01;

import java.util.Scanner;

public class Calculo {
    
    private int hours;
    
    public int Datos() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese sus horas de trabajo: ");
        int horas = sc.nextInt();
        return horas;
    }
    public int Calcular(int i) {
        hours = i;
        int pago = 0;
        if (hours <= 40 && hours >= 0) {
            pago = hours * 16;
        } else if (hours > 40) {
            pago = 640 + (hours-40)*20;
        } else {
            pago = -1;
        }        
        return pago;
    }
}
